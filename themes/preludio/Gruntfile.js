module.exports = function (grunt) {
    grunt.initConfig({
        uglify: {
            my_target: {
                files: {
                    'source/js/all.min.js':
                        [
                            'source/js/util/jquery/jquery-3.2.1.min.js',
                            'source/js/util/jquery/jquery-ui-1.12.1.min.js',
                            'source/js/util/sweetalert/sweetalert.js',
                            'source/js/util/jqmask.min.js',                            
                            'source/js/util/base.js'
                        ]
                }
            },
        },
        copy: {
            fontawesome: {
                expand: true,
                cwd: 'tmp/fontawesome/fonts/',
                src: ['**'],
                dest: 'source/css/fonts/'
            },
            fancybox: {
                expand: true,
                cwd: 'tmp/fancybox/source/',
                src: ['**'],
                dest: 'source/fancybox/'
            }
        },
        _clean: {
            tmp: ['tmp'],
            fontawesome: ['source/css/fonts'],
            fancybox: ['source/fancybox']
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.loadNpmTasks('grunt-contrib-uglify');    

  grunt.registerTask('default', ['uglify']);
};