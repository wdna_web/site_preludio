$(document).ready(function() {
    menuOnScroll();
    sideMenu();
    mask();
    menuItemScroll();
    onSendClick();
});

function menuOnScroll() {
    $(window).on("scroll", function() {
        var h = $(this).scrollTop();
        if (h >= 680) $("#icon-back-top").addClass("show");
        else $("#icon-back-top").removeClass("show");

        if (h == 0) $(".nav-full").removeClass("black");
        else $(".nav-full").addClass("black");
    });
}

function sideMenu() {
    $("#icon-menu").click(function() {
        openNavSide();
    });

    $("#icon-menu-close").click(function() {
        closeNavSide();
    });
}

function mask() {
    $("input#phone")
        .mask("(99) 99999-999?9")
        .focusout(function(event) {
            var target, phone, element;
            target = event.currentTarget
                ? event.currentTarget
                : event.srcElement;
            phone = target.value.replace(/\D/g, "");
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });
}

function openNavSide() {
    var w = window.outerWidth;
    if (w <= 777) {
        $(".overlay").css("width", "100%");
        $(".nav-full").toggleClass("opened");
        $(".nav-side__container").css("width", "100%");
    } else {
        $(".overlay").css("width", "100%");
        $(".nav-full").toggleClass("opened");
        $(".nav-side__container").css("width", "350px");
    }
}

function closeNavSide() {
    $(".overlay").css("width", "0");
    $(".nav-full").toggleClass("opened");
    $(".nav-side__container").css("width", "0");
}

function menuItemScroll() {
    var t = location.href;

    $("#link-home").click(function(t) {
        t.preventDefault();
        closeNavSide();
        $("html, body").animate(
            { scrollTop: $("#header").offset().top },
            1300,
            "easeInOutExpo"
        );
    });
    $("#link-subbanner").click(function(t) {
        t.preventDefault();
        closeNavSide();
        $("html, body").animate(
            { scrollTop: $("#subbanner").offset().top },
            1300,
            "easeInOutExpo"
        );
    });
    $("#link-storeparallax").click(function(t) {
        t.preventDefault();
        closeNavSide();
        $("html, body").animate(
            { scrollTop: $("#storeparallax").offset().top },
            1300,
            "easeInOutExpo"
        );
    });
    $("#link-aboutparallax").click(function(t) {
        t.preventDefault();
        closeNavSide();
        $("html, body").animate(
            { scrollTop: $("#aboutparallax").offset().top },
            1300,
            "easeInOutExpo"
        );
    });
    $("#link-contactparallax").click(function(t) {
        t.preventDefault();
        closeNavSide();
        $("html, body").animate(
            { scrollTop: $("#contactparallax").offset().top },
            1300,
            "easeInOutExpo"
        );
    });
    $("#icon-back-top").click(function(t) {
        t.preventDefault();        
        $("html, body").animate(
            { scrollTop: $("#header").offset().top },
            1300,
            "easeInOutExpo"
        );
    });
}

//CONTACT EMAIL
function sendEmail(data) {
    var apiUrl =
        "https://htw4edrz17.execute-api.us-west-2.amazonaws.com/prod/contact";
    $.ajax({
        type: "POST",
        url: apiUrl,
        ContentType: "application/json",
        headers: {
            "x-api-key": token
        },
        crossDomain: true,
        data: JSON.stringify(data),
        beforeSend: function() {
            swal({
                title: "Aguarde um momento.",
                html: "",
                text: "",
                onOpen: function() {
                    swal.showLoading();
                }
            });
        },
        success: function() {
            $("#form-contact").trigger("reset");
            swal({
                title: "Dados enviados com sucesso.",
                html: "",
                type: "success"
            });
        },
        error: function(error) {
            swal({
                title: "Não foi possível enviar seus dados.",
                text: "Tente mais tarde.",
                type: "error"
            });
        }
    });
}

function validateContact(data) {
    var emptyValue = "",
        limitcaracteres = "";

    for (var i in data) {
        if (data[i]["value"].length == 0 && data[i]["required"] == true) {
            emptyValue = emptyValue.concat(
                '<span class="swal2-validate">O campo <b>' +
                    data[i]["title"] +
                    "</b> é obrigatório.</span></br>"
            );
        } else if (data[i]["value"].length > data[i]["length"]) {
            limitcaracteres = limitcaracteres.concat(
                '<span class="swal2-validate">O campo <b>' +
                    data[i]["title"] +
                    "</b> deve conter no máximo " +
                    data[i]["length"] +
                    " caracteres.</span></br>"
            );
        }
        data[i] = data[i]["value"].replace(/<[^>]+>/gi, "");
    }
    if (emptyValue.length > 0 || limitcaracteres.length > 0) {
        var textempty = emptyValue.length > 0 ? emptyValue : "";
        var textCaracteres = limitcaracteres.length > 0 ? limitcaracteres : "";
        swal({
            html: textempty + textCaracteres,
            type: "warning"
        });
    } else {
        sendEmail(data);
    }
}

function onSendClick() {
    $("#form-contact").submit(function(event) {
        if ($("input#hidden").val().length == 0) {
            event.preventDefault();
            var data = {
                name: {
                    value: $("#name").val(),
                    title: "nome",
                    length: 50,
                    required: true
                },
                email: {
                    value: $("#email").val(),
                    title: "e-mail",
                    length: 200,
                    required: true
                },
                telefone: {
                    value: $("#phone").val(),
                    title: "telefone",
                    length: 15,
                    required: true
                },
                assunto: {
                    value: $("#subject").val(),
                    title: "assunto",
                    length: 15,
                    required: true
                },
                mensagem: {
                    value: $("#message").val(),
                    title: "mensagem",
                    length: 500,
                    required: true
                }
            };
            validateContact(data);
        }
        return false;
    });
}
